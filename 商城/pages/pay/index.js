/**
 * 页面加载的时候 
 *  1.从缓存中获取购物车数据 渲染到页面中
 *    这些数据 checked=true
 * 2.微信支付
 *  1.哪些人 哪些账号 可以实现微信支付
 *    企业账号
 *    企业账号后台小程序中必须给开发者 添加上白名单
 *      appid绑定多个开发者 这些开发者可以公用 appid 和它的开发权限
 *  2.支付按钮 
 *    1.先判断缓存中有没有token   
 *    2.没有 跳转到 授权页面 进行获取token
 *    3.有token
 *    4.创建订单
 */
import {request} from '../../request/index.js'
import regeneratorRuntime from '../../lib/runtime/runtime'
import {showModal,showToast,requestPayment} from '../../utils/asyncWx.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    address:[],
    cart:[],
    
    totalPrice:0,
    totalNum:0
  },
  onShow(){
    const address=wx.getStorageSync("address");
      //获取缓存 可能是空数组
    let cart=wx.getStorageSync("cart")||[];
    cart=cart.filter(v=>v.checked);

    
    let totalPrice=0;
    let totalNum=0;
    cart.forEach(v=>{
        totalPrice+=v.num*v.goods_price;
        totalNum+=v.num;
    })
    this.setData({cart,totalNum,totalPrice,address});
  },
  //点击 支付 授权
  async handleOrderPay(){
    try {
      const token=wx.getStorageSync("token");
    if(!token){
      wx.navigateTo({
        url: '/pages/auth/index'
      });
      return;
    }
    // console.log("已经存在token")
    //创建订单
    //准备请求头参数
    // const header={Authorization:token};
    //准备请求体参数
    const order_price=this.data.totalPrice;
    const consignee_addr=this.data.address.all;
    const cart=this.data.cart;
    let goods=[];
    cart.forEach(v=>goods.push({
      goods_id:v.goods_id,
      goods_number:v.num,
      goods_price:v.goods_price
    }))

    const orderParams={order_price,consignee_addr,goods};
    const {order_number}=await request({url:"/my/orders/create",method:"POST",data:orderParams});
    // console.log(order_number);
    //5.发起预支付
    const {pay}=await request({url:"/my/orders/req_unifiedorder",method:"POST",data:{order_number}})
    //6.发起微信支付
    await requestPayment(pay);
    //7.查询后台 订单状态  后台返回 支付成功
    const res=await request({url:"/my/orders/chkOrder",method:"POST",data:{order_number}});
    await showToast({title:"支付成功"});
    /**
     * 移除已支付的订单
     * let newCart=wx.getStorageSync("cart");
     * newCart=newCart.filer(v=>!v.checked);
     * wx.setStorageSync("cart",newCart)
     */
    } catch (error) {
      const {errMsg}=error;
      await showToast({title:"支付失败"+errMsg});

      console.log(error);
    }
   
  }
 

})
/**
 * 1.输入框绑定 值改变事件 input事件
 *  1.获取到输入框的值
 *  2.合法性判断
 *  3.校验通过 把输入框的值 发送给后台
 *  4.返回的数据打印到页面上
 * 2.防抖(防止抖动) 定时器
 *  0 防抖 一般 输入框中 防止重复输入 重复发送请求
 *  1.节流 一般用在页面下拉或者下拉
 *  1.定义全局的定时器 id
 */
import {request} from '../../request/index.js'
import regeneratorRuntime from '../../lib/runtime/runtime'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goods:[],
    isFocus:false
    
  },
  TimeId:-1,
  //输入框的值改变 就会触发的事件
  handleInput(e){
    // console.log(e) 获取输入的值
    const {value}=e.detail;
    //2.检测合法性
    if(!value.trim()){
      this.setData({
        goods:[],
        isFocus:false
      })
      //清除定时器
      clearTimeout(this.TimeId);
      return;
    }
    //发送请求 获取数据  清除定时器
    this.setData({isFocus:true})
    clearTimeout(this.TimeId);
    this.TimeId=setTimeout(()=>{
      this.qsearch(value)
    },1000)
    
  },
  //发送请求 获取数据
  async qsearch(query){
    const res=await request({url:"/goods/qsearch",data:{query}});
    // console.log(res)
    this.setData({goods:res})
  }
})
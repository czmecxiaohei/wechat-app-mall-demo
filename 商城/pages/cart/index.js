/**
 * 1.onshow()
 *  0.回到商品详情页面，第一次添加商品的时候 手动添加了属性
 *    1.num=1
 *    2.checked=true
 *  1.获取缓存中的购物车数组中
 *  2.把购物车数据，填充到data中
 * 2.全选的实现 数据的展示
 *  1.onshow 获取缓存中的购物车数组
 *  2.根据购物车中的商品数据 所有商品都被选中 checked=true 全选就被选中
 * 3.总价格和总数量
 *  1.都需要商品被选中 我们才拿他来计算
 *  2.获取购物车数组
 *  3.遍历
 *  4.判断商品是否被选中
 *  5.总价格+=商品单价*商品的数量
 *  总数量+=商品的数量
 *  6.把计算后的价格和数量 设置回data中即可
 * 4.商品的选中
 *  1.绑定change事件
 *  2.获取到被修改的商品对象
 *  3.商品对象的选中状态
 *  4.重新填充到data中与缓存中
 *  5.重新计算全选 总价格 总数量
 * 5.全选和反选 
 *  1.全选复选框绑定事件
 *  2.获取data 中的全选变量 allchecked
 *  3.直接取反 allchecked=！allchecked
 *  4.遍历购物车数组中的 商品状态 随着 allchecked 改变而改变
 *  5.把购物车数组和allchecked 重新设置回data 把购物车重新设置回缓存中 
 * 6.商品数量的编辑
 *  1."+" "-" 按钮 绑定一个事件 区分的关键 自定义属性 "+" +1 "-" -1
 *  2.传递被点击的商品id goods_id
 *  3.获取data中的购物车数组 来获取需要被修改的商品对象
 *  0.当购物车的数量=1  同时用户 点击 "-"
 *     弹窗提示(ws.showModal) 询问是否删除
 *      1.确定 直接执行删除
 *      2.取消 什么都不做
 *  4.直接修改商品对象的数量 num
 *  5.把cart数组 重新设置回 缓存中 和data this.setcart()
 */
import {request} from '../../request/index.js'
import regeneratorRuntime from '../../lib/runtime/runtime'
import {showModal,showToast} from '../../utils/asyncWx.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    address:[],
    cart:[],
    allChecked:false,
    totalPrice:0,
    totalNum:0
  },
  onShow(){
    const address=wx.getStorageSync("address");
      //获取缓存 可能是空数组
    const cart=wx.getStorageSync("cart")||[];
    
    this.setData({
      address
    });
    this.setCart(cart);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
    //点击 收货地址
  handleChooseAddress(){
    wx.chooseAddress({
      success: (result)=>{
        // console.log(result)
        const address=result;
        address.all=address.provinceName+address.cityName+address.countyName+address.detailInfo;
        
        wx.setStorageSync("address",address)
      }
    })
  },
  //商品的选中
  handleItemChange(e){
    //1.获取到被修改的商品id
    const goods_id=e.currentTarget.dataset.id;
    //2.获取购物车 数组
    let {cart}=this.data;
    //3.找到商品
    let index=cart.findIndex(v=>v.goods_id===goods_id)
    //4.选中状态下 反选
    cart[index].checked=!cart[index].checked;
 
    this.setCart(cart);
  },
  //设置购物车状态的同时 重新计算 底部工具栏的数据 全选 总价格 购买的数量 封装
  setCart(cart){

    let allChecked=true;
    let totalPrice=0;
     let totalNum=0;
     cart.forEach(v=>{
       if(v.checked){
         totalPrice+=v.num*v.goods_price;
         totalNum+=v.num;
       }else{
         allChecked=false;
       }
     })
     //判断数组是否为空
     allChecked=cart.length!=0?allChecked:false;
     this.setData({cart,allChecked,totalNum,totalPrice});
     wx.setStorageSync("cart",cart);
  },
  //商品的全选功能
  handleItemAllCheck(){
    //1.获取data的数据
    let {cart ,allChecked}=this.data;
    //2.修改状态
    allChecked=!allChecked;
    //3.循环修改 cart数组中的checked 状态
    cart.forEach(v=>v.checked=allChecked);
    //重新填充回缓存中
    this.setCart(cart);
  },
  //商品数量的编辑功能
  async handleItemNumEdit(e){
    //1.获取传递过来的参数
    const {operation,id}=e.currentTarget.dataset;
    //2.获取购物车数组
    let {cart}=this.data;
    //3.找到需要修改的商品的索引
    const index=cart.findIndex(v=>v.goods_id===id);
    //4.判断是否执行删除
    if(cart[index].num===1&&operation===-1){     
      const res=await showModal({content:"你是否要要执行删除"})
      if(res.confirm){
        cart.splice(index,1);
        this.setCart(cart);
      }
    }else{
       //4.进行修改数量
    cart[index].num+=operation;
    //设置回缓存和data中
    this.setCart(cart);
    }
    
  },
  //点击 结算
  async handlePay(){
    const {address ,totalNum}=this.data;
    //1.判断收货地址
    if(!address.userName){
      await showToast({title:"你还没有添加收货地址"});
      return;
    }
    //2.判断用户有没有选购商品
    if(totalNum===0){
      await showToast({title:"你还没有选购商品"});
      return;
    }
    //跳转到支付页面
    wx.navigateTo({
      url: '/pages/pay/index'
    });
  }
})
/**
 * 1.点击“+” 触发tap点击事件
 *  1.获取到图片的路径 选择图片的api
 *  2.获取到图片的路径 数组
 *  3.把图片路径 存到data的变量中
 *  4.页面就可以根据 图片数组进行循环显示 自定义组件
 * 
 * 2.点击 自定义组件
 *  1.获取被点击的元素的索引
 *  2.获取data中的图片数组
 *  3.根据索引 数组中删除 对应的元素
 *  4.把数组重新设置为data中
 * 
 * 3.点击“提交”
 *  1.获取文本域的内容 类似 输入框的获取
 *    1.data中定义变量 表示输入框内容
 *    2.文本域 绑定 输入事件 事件触发的时候 把输入框的值 存入到 变量中
 *  2.对这些内容进行合法验证
 *  3.验证通过 用户选择的图片 上传到专门 的图片的服务器 返回图片外网的链接
 *      1.遍历图片数组
 *      2.挨个上传
 *      3.自己再维护图片数组 存放  图片上传后的外网链接
 *  4.文本域和外网的图片的路径 一起提交到服务器 前端的模拟 不会发送到后台
 *  5.清空当前页 
 *  6.返回上一级
 */
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs:[
      {
        id:0,
        value:"体验问题",
        isActive:true
      },
      {
        id:1,
        value:"商品、商家投诉",
        isActive:false
      }
    ],
    //被选中的图片路径 数组
  chooseImgs:[],
  //文本域的内容
  textVal:""
  },
 
  UpLoadImgs:[],
  handleTabsItemChange(e){
    const {index}=e.detail;
    let {tabs}=this.data;

    tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);

    this.setData({
      tabs
    })
  },
  //点击+ 选择图片
  handleChooseImg(){
    wx.chooseImage({
      //数量最大为9
      count: 9,
      //图片的格式
      sizeType: ['original','compressed'],
      //图片的来源 相册 照相机
      sourceType: ['album','camera'],
      success: (result)=>{
      //  console.log(result) 
      this.setData(
        {
          //对图片进行拼接
          chooseImgs:[...this.data.chooseImgs,...result.tempFilePaths]
        }
      )
      }
    });
  },
  //点击 自定义组件
  handleRemoveImg(e){
    const {index}=e.currentTarget.dataset;
    // console.log(index)
    let {chooseImgs}=this.data;
    chooseImgs.splice(index,1);
    this.setData({chooseImgs})
  },
  //文本域的输入的事件
  handleTextInput(e){
    this.setData({
      textVal:e.detail.value
    })
  },
  //提交表单
  handleFromSubmit(){
    const {textVal,chooseImgs}=this.data;
    if(!textVal.trim()){
      wx.showToast({
        title: '输入不合法',
        icon: 'none',
        mask: true,
      });
      return;
    }
    wx.showLoading({
      title: '正在上传中',
      mask: true,
    });
    if(chooseImgs.length!=0){
    //准备上传图片 到专门的图片服务器 
    chooseImgs.forEach((v,i)=>{
        //图床地址 img.coolcr.cn/index/api.html
      wx.uploadFile({
      // 图片上传到的位置
      url: 'https://img.coolcr.cn/api/upload',
      // 被上传的文件的路径
      filePath: v,
      // 上传的文件的名称 后台来获取文件 file
      name: "image",
      // 顺带的文本信息
     formData: {},
     success: (result)=>{
       let url=JSON.parse(result.data).url;
       this.UpLoadImgs.push(url);
       //所有的图片送上传了才触发了
       if(i===chooseImgs.length-1){
         wx.hideLoading();
         //重置页面
         console.log("图片和文本都上传上去");
         this.setData({
           textVal:"",
           chooseImage:[]
         });
         wx.navigateBack({
           delta: 1
         });
       }
     }
   });
    })
  }
  else{
    wx.hideLoading();
    console.log("只是提交了文本")
    wx.navigateBack({
      delta: 1
    });
  }
  }



})
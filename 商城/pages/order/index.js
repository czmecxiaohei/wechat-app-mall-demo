/**
 * 1.页面被打开的时候 onshow
 *  0 onshow 不同于onload 无法在形参上接收 options参数 
 *  0.5 判断缓存中有没有授权
 *    1.没有 直接跳转到授权页面
 *    2.有 直接往下进行
 *  1.获取url 上的参数type
 *  2.根据type 去发送请求获取订单数据
 *  3.渲染页面
 * 2.点击不同的标题 重新发送请求来获取和渲染数据
 */
import {request} from '../../request/index.js'
import regeneratorRuntime from '../../lib/runtime/runtime'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs:[
      {
        id:0,
        value:"全部",
        isActive:true
      },
      {
        id:1,
        value:"待付款",
        isActive:false
      },
      {
        id:2,
        value:"待发货",
        isActive:false
      },
      {
        id:3,
        value:"退款/退货",
        isActive:false
      },
    ],
    orders:[]
  },

  handleTabsItemChange(e){
    const {index}=e.detail;
    this.changeTitleByIndex(index);
    this.getOrders(index+1);
   
    // console.log(e)
  },
  //根据标题索引来激活选中 标题数组
  changeTitleByIndex(index){
    let {tabs}=this.data;
    tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);
    this.setData({
      tabs
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    const token=wx.getStorageSync("token");
    if(!token){
      wx.navigateTo({
        url: '/pages/auth/index'
      });
      return;
    }
    // 获取小程序的页面栈-数组 长度最大时10页面
    let pages=getCurrentPages();
    //2.数组中，索引最大的页面就是当前页
    let currentPage=pages[pages.length-1];
    console.log(currentPage.options);
    //3.获取url中的type参数
    const {type}=currentPage.options;
    //4.激活选中页面的标题
    this.changeTitleByIndex(type-1);
    this.getOrders(type);
  },
  //获取订单列表的方法
  async getOrders(type){
    const res=await request({url:"/my/orders/all",data:{type}});
   this.setData({orders:res.orders.map(v=>({...v,create_time_cn:(new Date(v.create_time*1000).toLocaleString())}))});

  },
  

 
})
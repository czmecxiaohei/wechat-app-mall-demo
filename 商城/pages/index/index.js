//Page Object
import{request} from '../../request/index.js'
import regeneratorRuntime from '../../lib/runtime/runtime'
Page({
  data: {
    //轮播数组数据
    swipperList:[],
    //分类
    catesList:[],
    //楼层数据
    floorList:[]
  },
  //options(Object)
  onLoad: function(options){
    //1.轮播图 优化的手段可以通过es6的promise来解决这个问题
    this.getSwiperList();
    this.getCateList();
    this.getFloorList();
  },
  //获取轮播图数据
  getSwiperList(){
    request({url:'/home/swiperdata'})
    .then(result=>{
      result.forEach((v, i) => {result[i].navigator_url = v.navigator_url.replace('main', 'index');});
      this.setData({
        
              swipperList:result
            }) 
    });
    
  },
  //获取分类图数据
  getCateList(){
    request({url:'/home/catitems'})
    .then(result=>{
      this.setData({
              catesList:result
            })
    })
  },
  //获取楼层数据
  getFloorList(){
    request({url:'/home/floordata'})
    .then(result=>{
      for (let k = 0; k < result.length; k++) {
        result[k].product_list.forEach((v, i) => {
            result[k].product_list[i].navigator_url = v.navigator_url.replace('?', '/index?');
        });
    }
      this.setData({
              floorList:result
            })
    })
  },
 

});
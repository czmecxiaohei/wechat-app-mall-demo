import {request} from '../../request/index.js'
import regeneratorRuntime from '../../lib/runtime/runtime'
import {login,showToast} from '../../utils/asyncWx.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  onShow: function () {

  },
  async handleGetUserInfo(e){
    try {
      // console.log(e)
      //1.获取用户信息
      const {encryptedData,rawData,iv,signature}=e.detail;
      //2.获取小程序登录成功后的code
      const {code}=await login();
      // 自己写的token 不是传给后台的token
      const token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo";
      const loginParams={encryptedData,rawData,iv,signature,code};
      const res=await request({url:"/users/wxlogin",data:loginParams,method:"post"})
      // console.log(res);res为null token为null
      wx.setStorageSync("token", token);
      wx.navigateBack({
        delta: 1
      });
    } catch (error) {
      console.log(error);
       // 弹框提示
       showToast({title:'获取 token 失败，无法实现支付功能'});
      // 返回到上一级页面
      setTimeout(() => {
          wx.navigateBack({
              delta: 1
            })
      }, 1500);
    }
      
  }
 
})